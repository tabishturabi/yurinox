from odoo import http, _
import json
from odoo.http import request
from datetime import date, datetime, timedelta
from odoo.exceptions import UserError, ValidationError


class GetAllProducts(http.Controller):

    # @http.route('/get_all_products_json', type='http', auth='public', website=True)
    # def get_all_products(self):
    #     # Fetch all products
    #     products = request.env['product.template'].sudo().search([])
    #
    #     # Prepare product data
    #     product_data = []
    #     product_variants = []
    #     product_variants_attributes = []
    #     for product in products:
    #
    #         for variant in product.product_variant_ids:
    #             for attribute in variant.product_template_attribute_value_ids:
    #                 product_variants_attributes.append(attribute.id)
    #             variants_obj = request.env['product.template.attribute.value'].sudo().browse(set(product_variants_attributes))
    #             variants_val = {}
    #             sizes = []
    #             colors = []
    #             height = []
    #             for rec in variants_obj:
    #                 if rec.attribute_id.name == 'Sizes':
    #                     sizes.append(rec.name)
    #                 if rec.attribute_id.name == 'Colors':
    #                     colors.append(rec.name)
    #                 if rec.attribute_id.name == 'Height':
    #                     height.append(rec.name)
    #
    #             product_variants.append({
    #                 'default_code': variant.default_code,
    #                 'variant_name': variant.name,
    #                 'name': product.name,
    #                 'product_variants_attributes': {
    #                     "Sizes": ",".join(sizes),
    #                     "Colors": ",".join(colors),
    #                     "Height": ",".join(height),
    #                 },
    #                 'availability': variant.qty_available,
    #                 'wholesale_price': variant.wholesale_price,
    #                 'base_price': variant.lst_price,
    #                 # Add more fields as needed
    #             })
    #
    #         product_data.append({
    #             'default_code': product.default_code,
    #             'bitrix_product_type': product.bitrix_product_type,
    #             'name': product.name,
    #             'product_variants': product_variants,
    #             # Add more fields as needed
    #         })
    #
    #     # Convert product data to JSON
    #     json_data = json.dumps(product_data, indent=4)
    #     # Set appropriate content type
    #     # headers = {'Content-Type': 'application/json'}
    #     file_name = 'products_with_variants-{}.json'.format(datetime.strftime(datetime.now(), '%Y%m%d%H%M%S%f'))
    #
    #     headers = {
    #         'Content-Disposition': 'attachment; filename='+file_name,
    #         'Content-Type': 'application/json',
    #     }
    #
    #     # Return JSON response
    #     return http.Response(json_data, headers=headers)

    @http.route('/get_all_products_json', type='http', auth='public', website=True)
    def get_all_products(self):
        # Fetch all products
        products = request.env['product.product'].sudo().search([])

        # Prepare product data
        product_data = []

        for product in products:
            product_data.append({
                'default_code': product.default_code,
                'bitrix_product_type': product.bitrix_product_type,
                'name': product.display_name,
                'availability': product.qty_available,
                'wholesale_price': product.wholesale_price,
                'base_price': product.lst_price,
            })
        # Convert product data to JSON
        json_data = json.dumps(product_data, indent=4)
        # Set appropriate content type
        # headers = {'Content-Type': 'application/json'}
        file_name = 'products_with_variants-{}.json'.format(datetime.strftime(datetime.now(), '%Y%m%d%H%M%S%f'))

        headers = {
            'Content-Disposition': 'attachment; filename=' + file_name,
            'Content-Type': 'application/json',
        }

        # Return JSON response
        return http.Response(json_data, headers=headers)

    @http.route('/get_all_products_json/<int:export_product_bitrix_id>', type='http', auth='public', website=True)
    def get_all_products_by_export_id(self, export_product_bitrix_id):
        # Fetch all products
        products = request.env['export.products.bitrix'].sudo().browse(export_product_bitrix_id).product_ids.mapped(
            'product_tmpl_id')
        if products:
            # Prepare product data
            product_data = []
            product_variants = []
            product_variants_attributes = []
            for product in products:
                product_data.append({
                    'default_code': product.default_code,
                    'bitrix_product_type': product.bitrix_product_type,
                    'name': product.display_name,
                    'availability': product.qty_available,
                    'wholesale_price': product.wholesale_price,
                    'base_price': product.lst_price,
                })

            # Convert product data to JSON
            json_data = json.dumps(product_data, indent=4)
            # Set appropriate content type
            # headers = {'Content-Type': 'application/json'}
            file_name = 'products_with_variants-{}.json'.format(datetime.strftime(datetime.now(), '%Y%m%d%H%M%S%f'))

            headers = {
                'Content-Disposition': 'attachment; filename=' + file_name,
                'Content-Type': 'application/json',
            }

            # Return JSON response
            request.env['export.products.bitrix'].sudo().browse(export_product_bitrix_id).write({'state': 'sent'})
            return http.Response(json_data, headers=headers)
        else:
            raise ValidationError(_('No Product Found.'))

    @http.route('/get_all_products_with_stock_json/<int:export_product_stock_bitrix_id>', type='http', auth='public',
                website=True)
    def get_all_products_with_stock_export_id(self, export_product_stock_bitrix_id):
        # Fetch all products
        products = request.env['export.stock.bitrix'].sudo().browse(export_product_stock_bitrix_id).product_ids
        if products:
            # Prepare product data
            product_data = []
            product_variants = []
            for product in products:
                product_variants.append({
                    'default_code': product.default_code,
                    'display_name': product.display_name,
                    'qty_available': product.qty_available,
                })

            json_data = json.dumps(product_variants, indent=4)
            file_name = 'products_stock_variants-{}.json'.format(datetime.strftime(datetime.now(), '%Y%m%d%H%M%S%f'))

            headers = {
                'Content-Disposition': 'attachment; filename=' + file_name,
                'Content-Type': 'application/json',
            }

            # Return JSON response
            request.env['export.stock.bitrix'].sudo().browse(export_product_stock_bitrix_id).write({'state': 'sent'})
            return http.Response(json_data, headers=headers)
        else:
            raise ValidationError(_('No Product Found.'))

    @http.route('/create_sale_order', auth='public', type='json', csrf=False)
    def create_sale_order(self, **post):
        payload = json.loads(request.httprequest.data)
        env = http.request.env
        response = {'status': 'success', 'message': 'Sale orders created successfully.', 'details': []}

        transaction = payload.get('body', {})
        customer_data = transaction.get('customer_data', {})
        phone_number = customer_data.get('phone_number')
        email = customer_data.get('email')
        customer = False
        if email:
            customer = env['res.partner'].search([('email', '=', email)], limit=1)

        if not customer:
            state_name = customer_data.get('address', {}).get('state')
            state = env['res.country.state'].search([('code', '=', state_name)], limit=1)
            state_id = state.id if state else False
            customer_vals = {
                'name': f"{customer_data.get('first_name', '')} {customer_data.get('last_name', '')}".strip(),
                'phone': phone_number,
                'city': customer_data.get('address', {}).get('city'),
                'state_id': state_id,
                'zip': customer_data.get('address', {}).get('zip'),
                'email': email
            }
            customer = env['res.partner'].sudo().create(customer_vals)

        order_lines = []
        product_not_found = False
        for item in transaction.get('basket', []):
            product_bitrix_id = item.get('bitrix_product_id')
            product = env['product.product'].sudo().search([('default_code', '=', str(product_bitrix_id))], limit=1)
            if not product:
                response['status'] = 'error'
                response['message'] = 'Some orders were not created due to errors.'
                error_message = f"The product with reference {product_bitrix_id} is not available."
                response['details'].append({'transaction': transaction, 'error': error_message})
                product_not_found = True
                break

            order_lines.append((0, 0, {
                'product_id': product.id,
                'product_uom_qty': item.get('quantity', 1),
                'price_unit': item.get('unit_price', 0.0)
            }))

            if product_not_found:
                continue

            order_vals = {
                'partner_id': customer.id,
                'order_line': order_lines,
                'bitrix_payload': json.dumps(transaction, indent=4, sort_keys=True)
            # Save the payload data in bitrix_payload field
            }
            env['sale.order'].sudo().create(order_vals)

        if response['status'] == 'success':
            response['message'] = 'Sale orders created successfully.'

        return response
