# -*- coding: utf-8 -*-
from odoo import http
import json
import requests
from datetime import datetime
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from werkzeug.urls import url_encode


class ExportProductsBitrix(models.Model):
    _name = 'export.products.bitrix'
    _inherit = ['mail.thread']
    _order = 'create_date desc'
    _description = 'Export Products Bitrix '

    name = fields.Char(string='Document Reference', readonly=True)
    products_count = fields.Integer(string='Products Count', track_visibility='onchange')
    state = fields.Selection([
        ('draft', 'Draft'), ('sent', 'Sent'), ('cancelled', 'Cancelled')
    ], string='State', default='draft', track_visibility='onchange')
    product_ids = fields.Many2many('product.product', string='Products', readonly=True)

    def action_export(self):
        url = 'http://localhost:8069/get_all_products_json'  # Replace with your API endpoint URL
        # headers = {'Authorization': 'Bearer YOUR_API_KEY'}  # Replace with your API key or authentication token
        # payload = {'param1': 'value1', 'param2': 'value2'}  # Replace with your request payload if needed
        self.ensure_one()
        if self.product_ids:
            return {
                'name': _('Export Products Json'),
                'type': 'ir.actions.act_url',
                'url': '/get_all_products_json/'+str(self.id),
            }
        else:
            raise ValidationError(_('No Product Found.'))

    def action_cancel(self):
        if self:
            self.state = 'cancelled'


class ExportProductsBitrixWizard(models.TransientModel):
    _name = 'export.products.bitrix.wizard'
    _order = 'create_date desc'

    name = fields.Char(string='Name', default='Export Products to Bitrix')
    note = fields.Text(string='Important Note', default="Important Note:\nThis operation exports existing products to "
                                                        "Bitrix website. Use options below to select products to be "
                                                        "exported.")
    product_category_ids = fields.Many2many('product.category', string='Set specific Product Category')
    can_be_sold_only = fields.Boolean(string='Can be Sold products only', default=True)

    def export_products(self):
        model_name = self._context.get('model_name')
        products = self.env['product.product'].search([])
        if self.product_category_ids:
            category_ids = self.product_category_ids.ids
            products = products.filtered(lambda p: any(category_id in p.categ_id.ids for category_id in category_ids))
        if self.can_be_sold_only:
            products = products.filtered(lambda p: p.sale_ok)

        export_product_vals = {
            'create_uid': self.env.user.id,
            'name': f"EXP-{datetime.now().strftime('%Y%m%d')}/{self.id}",
            'create_date': fields.Datetime.now(),
            'products_count': len(products),
            'state': 'draft',
            'product_ids': [(6, 0, products.ids)]  # Assuming product_ids is a many2many field in export.products.bitrix
        }
        export_product = self.env[model_name].create(export_product_vals)
        log_line_vals = {
            'user_ref': self.env.user.id,
            'details': 'Export Product Bitrix operation has been successfully completed.',
            'doc_ref': 'Export Product Bitrix',
            'doc_ref_id': export_product.id,
        }
        self.env['britrix.log.lines'].create(log_line_vals)
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }
