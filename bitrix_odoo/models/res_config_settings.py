# -*- coding: utf-8 -*-
from odoo import http
import json
import requests
from datetime import datetime
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from werkzeug.urls import url_encode



class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    stock_field = fields.Selection(
        [('free_to_use', 'Free to Use Quantity'), ('in_hand', 'Quantity on Hand'),
         ('forecasted', 'Forecasted Quantity')],
        string="Export Stock Field",
        config_parameter='bitrix_odoo.stock_field')