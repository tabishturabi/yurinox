# -*- coding: utf-8 -*-
from odoo import http
import json
from datetime import datetime
from odoo import api, fields, models, _


class LogLines(models.Model):
    _name = 'britrix.log.lines'
    _description = 'Log Lines'

    doc_ref_id = fields.Integer(string='Document Reference')
    doc_ref = fields.Char(string='Document Reference')
    user_ref = fields.Many2one('res.users', string='User Reference', required=True)
    create_date = fields.Datetime(string='Creation Date', default=lambda self: fields.Datetime.now())
    details = fields.Text(string='Details')

    def create_log_entry(self, user_id, details, doc_ref, doc_ref_id):
        log_line = self.env['britrix.log.lines'].create({
            'user_ref': user_id,
            'details': details,
            'doc_ref': doc_ref,
            'doc_ref_id': doc_ref_id,
        })
        return log_line
