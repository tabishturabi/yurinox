# -*- coding: utf-8 -*-
from odoo import http
import json
import requests
from datetime import datetime
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from werkzeug.urls import url_encode


class ExportStockBitrix(models.Model):
    _name = 'export.stock.bitrix'
    _inherit = ['mail.thread']
    _order = 'create_date desc'
    _description = 'Export Stock Bitrix '

    name = fields.Char(string='Document Reference', readonly=True)
    products_count = fields.Integer(string='Products Count', track_visibility='onchange')
    state = fields.Selection([
        ('draft', 'Draft'), ('sent', 'Sent'), ('cancelled', 'Cancelled')
    ], string='State', default='draft', track_visibility='onchange')
    product_ids = fields.Many2many('product.product', string='Products', readonly=True)

    def action_export(self):
        url = 'http://localhost:8069/get_all_products_json'  # Replace with your API endpoint URL
        # headers = {'Authorization': 'Bearer YOUR_API_KEY'}  # Replace with your API key or authentication token
        # payload = {'param1': 'value1', 'param2': 'value2'}  # Replace with your request payload if needed
        self.ensure_one()
        if self.product_ids:
            return {
                'name': _('Export Stock Json'),
                'type': 'ir.actions.act_url',
                'url': '/get_all_products_with_stock_json/'+str(self.id),
            }
        else:
            raise ValidationError(_('No Product Found.'))

    def action_cancel(self):
        if self:
            self.state = 'cancelled'
