
/** @odoo-module **/
import { patch } from "@web/core/utils/patch";

import { ControlPanel } from "@web/search/control_panel/control_panel";

import { useService } from "@web/core/utils/hooks";

patch(ControlPanel.prototype, {
    setup(...args) {
        super.setup(...args);
        this.bitrix = true;
    },

 async onClickExportBitrix() {
        this.env.services.action.doAction({
            name: "Export Products For Bitrix",
            type: "ir.actions.act_window",
            res_model: "export.products.bitrix.wizard",
            view_mode: "form",
            views: [[false, "form"]],
            target: "new",
            context: {},
        });
    }

});



