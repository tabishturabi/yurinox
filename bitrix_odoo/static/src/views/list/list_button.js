/** @odoo-module */

import { ListController } from "@web/views/list/list_controller";
import { listView } from "@web/views/list/list_view";
import { registry } from "@web/core/registry";
const viewRegistry = registry.category("views");


export class BitrixlListView extends ListController {
    setup() {
        super.setup();
//        this.orm = useService("orm");
    }

async onClickExportBitrix() {
        console.log(this);
       var resModel = this.props.resModel;
       var display_name = "";
       var note = "";
       if(resModel == 'export.stock.bitrix')
       {
            display_name = "Export Stock For Bitrix";
            note = "Important Note:\nThis operation exports stock of existing products to Bitrix website. Use options below to filter products. Check Settings and specify the parameter “Export Stock Field” if needed."
       }
       if(resModel == 'export.products.bitrix')
       {
            display_name = "Export Products For Bitrix";
            note = "Important Note:\nThis operation exports existing products to Bitrix website. Use options below to select products to be exported."

       }
       this.env.services.action.doAction({
           name: display_name,
           type: "ir.actions.act_window",
           res_model: "export.products.bitrix.wizard",
           view_mode: "form",
           views: [[false, "form"]],
           target: "new",
           context: {
           'model_name': resModel,
           'default_name': display_name,
           'default_note': note,
           },
       });
   }

}

registry.category("views").add("button_in_tree", {
    ...listView,
    Controller: BitrixlListView,
    buttonTemplate: "bitrix_odoo.BitrixlListView.buttons",
});