# -*- coding: utf-8 -*-

{
    'name': 'Bitrix Odoo',
    'version': '17.0',
    'category': 'Social',
    'summary': 'bitrix_odoo_integration',
    'description': """Bitrix Odoo""",
    'author': 'Developer',
    'website': '',
    'depends': ['base', 'mail', 'product','stock','generate_product_json_data'],
    'data': [

        # Security files
        'security/security.xml',
        'security/ir.model.access.csv',
        # Data files
        'data/menu.xml',
        # View files
        'views/britrix_integration.xml',
        'views/export_stock.xml',
        'views/res_config_settings.xml',

    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'AGPL-3',
    'assets': {
        'web.assets_backend': [
            'bitrix_odoo/static/src/views/list/*',
            'bitrix_odoo/static/src/components/**/*',

        ],
    },

}