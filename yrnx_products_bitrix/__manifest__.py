{
    'name': 'Yurinox Bitrix Products',
    'version': '17.0',
    'category': "",
    'summary': """""",
    'description': """""",
    'depends': ['base', 'product','sale'],
    'data': [
        #'security/ir.model.access.csv',
        #'views/sftp_syncing.xml',
        'views/product_template.xml',
        'views/product_product.xml',
        'views/sale_order.xml',
    ],
    'images': [
    ],
    'demo': [],
}
