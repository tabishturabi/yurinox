from odoo import models, fields, _


class SaleOrder(models.Model):
    _inherit = "sale.order"

    bitrix_payload = fields.Json(store=True)
