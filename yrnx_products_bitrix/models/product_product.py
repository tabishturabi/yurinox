from odoo import models, fields, _
from odoo.exceptions import ValidationError, UserError


class ProductProduct(models.Model):
    _inherit = "product.product"

    wholesale_price = fields.Monetary(
        'Wholesale Price', default=0.0, copy=False,readonly=False,
        digits='Product Price',related='product_tmpl_id.wholesale_price'
    )
