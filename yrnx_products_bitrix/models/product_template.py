from odoo import models, fields, _
from odoo.exceptions import ValidationError, UserError


class ProductTemplate(models.Model):
    _inherit = "product.template"

    bitrix_detailed_image_url = fields.Text("Bitrix Detailed Image URL", copy=False, readonly=False)
    bitrix_product_type = fields.Char("Bitrix Product Type", copy=False, readonly=False)
    bitrix_sole_material = fields.Char("Bitrix Sole Material", copy=False, readonly=False)
    bitrix_upper_material = fields.Char("Bitrix Upper Material", copy=False, readonly=False)
    wholesale_price = fields.Monetary(string='Wholesale Price', default=0.0, copy=False,
        digits='Product Price',
    )